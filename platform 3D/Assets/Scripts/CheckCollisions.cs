﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CheckCollisions : MonoBehaviour {
	
	public string niveauSuivant;

	void Start()
	{
		AdBuddizBinding.SetAndroidPublisherKey("c248264d-4639-4f32-9007-c2e7342e7151");
		AdBuddizBinding.CacheAds();
	}

	void OnCollisionEnter(Collision obj)
	{
		if(obj.gameObject.name == "vide") {
			if(Random.Range(0,10) >= 7) {
				AdBuddizBinding.ShowAd();
			}
			
			SceneManager.LoadScene(Application.loadedLevelName);
		}

		else if(obj.gameObject.name == "fin") {
			AdBuddizBinding.ShowAd();
			if (niveauSuivant != "menu principal") {
				PlayerPrefs.SetString("dernierNiveau", niveauSuivant);
			}

			SceneManager.LoadScene(niveauSuivant);
		}
	}
}