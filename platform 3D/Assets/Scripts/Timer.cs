﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	
	public float myTimer = 00.0f;
	
	void Update()
	{
			myTimer += Time.deltaTime;
			this.gameObject.GetComponent<Text>().text = ""+(int)myTimer;
	}
}
