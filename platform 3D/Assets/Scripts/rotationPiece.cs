﻿using UnityEngine;
using System.Collections;

public class rotationPiece : MonoBehaviour {
	
	public int vitesse = 100;
	
	void Update()
	{
		transform.Rotate(Vector3.up * Time.deltaTime * vitesse);
	}
}