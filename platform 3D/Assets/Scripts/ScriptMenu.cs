﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScriptMenu : MonoBehaviour {

	private string dernierNiveau;

	void Start()
	{
		dernierNiveau = PlayerPrefs.GetString("dernierNiveau");
		if(dernierNiveau == "" || dernierNiveau == null) {
			dernierNiveau = "Jeu";
		}
	}

	public void recommencer(string niveau)
	{
		SceneManager.LoadScene(niveau);
	}

	public void chargerNiveau(string niveau)
	{
		SceneManager.LoadScene(dernierNiveau);
	}

	public void quitterJeu()
	{
		Application.Quit();
	}
}