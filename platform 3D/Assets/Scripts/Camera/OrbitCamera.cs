﻿using UnityEngine;
using System.Collections;

public class OrbitCamera : MonoBehaviour
{
	public Transform target;
	public float distance = 20.0f;
	public float xSpeed = 1f;
	public float ySpeed = 1f;
	private float x = 0.0f;
	private float y = 0.0f;
	private Vector3 distanceVector;

	void Start ()
	{
		distanceVector = new Vector3 (0.0f, 0.0f, -distance);
		Vector2 angles = this.transform.localEulerAngles;
		x = angles.x;
		y = angles.y;
		this.Rotate(x, y);
	}
	void Update ()
	{
		if (target) {
			this.RotateControls ();
		}
	}
	void RotateControls ()
	{
		if (Input.GetButton ("Fire1")) {
			x += Input.GetAxis ("Mouse X") * xSpeed;
			y += -Input.GetAxis ("Mouse Y") * ySpeed;
			Vector2 angles = this.transform.localEulerAngles;
			angles.x = x;
			angles.y = y;
			this.Rotate (x, y);
		}
		else {
			this.mouvment ();
		}
	}
	void Rotate (float x, float y)
	{
		Quaternion rotation = Quaternion.Euler (y, x, 0.0f);
		Vector3 position = rotation * distanceVector + target.position;
		transform.rotation = rotation;
		transform.position = position;
	}
	void mouvment ()
	{
		Vector3 position = distanceVector + target.position;
		transform.position = position;
	}
}